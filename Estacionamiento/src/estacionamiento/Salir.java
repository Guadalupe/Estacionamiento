
package estacionamiento;



import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;

public class Salir {
    JComboBox pos= new JComboBox();
    JLabel lpos= new JLabel("Lugar de estacionamiento: ");
    JComboBox marca= new JComboBox();
    JLabel lmarca= new JLabel("Marca del auto: ");
    JButton s= new JButton();
    JFrame salir= new JFrame();
    
    Auto a = new Auto();
    
    public void Salir(){
       
        
        
        salir.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        salir.setSize(500,400);
        salir.setVisible(true);
        salir.setLayout(null);
        
        lmarca.setBounds(20,0,300,50);
        salir.add(lmarca);
        
        lpos.setBounds(20,30,300,50);
        salir.add(lpos);
        
        marca.setBounds(190, 15, 200, 25);
        
        pos.setBounds(190, 45, 200, 25);
        llenar();
        
        marca.enable(false);
        
        s.setText("Salir de estacionamiento");
        s.setBounds(190,100,200,30);
        salir.add(s);
        
        s.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                a.salir((Integer)pos.getSelectedItem());
                salir.dispose();
            }
        });
        
        pos.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                marca.setSelectedIndex(pos.getSelectedIndex());
            }
        });
        
        
    }
    
    public void llenar(){

        for(int i=0; i<a.l.size(); i++){
            pos.addItem(a.l.get(i));
            marca.addItem(a.m.get(i));
        }
        salir.add(pos);
        salir.add(marca);
        
    }
    
}
