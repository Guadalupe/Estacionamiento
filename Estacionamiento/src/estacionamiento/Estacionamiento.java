
package estacionamiento;


import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;


public class Estacionamiento {
    
    public static void main(String args[]){
        int i=0, f,c;
        Auto a= new Auto();
        JFrame menu= new JFrame();
        
        menu.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        menu.setSize(400,400);
        menu.setVisible(true);
        menu.setLayout(null);
        JLabel l1= new JLabel("¿Qué decea realizar?");
        l1.setBounds(120,20,300,100);
        menu.add(l1);
        
        JButton ing= new JButton();
        ing.setText("Ingresar");
        ing.setBounds(120,100,150,30);
        menu.add(ing);
        
        JButton salir= new JButton();
        salir.setText("Salir");
        salir.setBounds(120,150,150,30);
        menu.add(salir);
        
        JButton ocupados= new JButton();
        ocupados.setText("Sitios ocupados");
        ocupados.setBounds(120,200,150,30);
        menu.add(ocupados);
        
        ing.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
               Ingresar l= new Ingresar();
               l.Lugar();
            }
        });
        
        salir.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Salir s= new Salir();
                s.Salir();
            }
        });
        
        ocupados.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                a.Lugares_ocupados();
            }
        });
    }
    
}
