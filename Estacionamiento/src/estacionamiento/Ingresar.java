
package estacionamiento;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;


public class Ingresar{
    
    JComboBox pos= new JComboBox();
    JLabel lpos= new JLabel("Lugar de estacionamiento: ");
    JLabel lmarca= new JLabel("Marca del auto: ");
    JButton ing= new JButton();
    
    Auto a= new Auto();
    JFrame lugar= new JFrame();
    
    public void Lugar(){
        
        
        
        lugar.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        lugar.setSize(500,400);
        lugar.setVisible(true);
        lugar.setLayout(null);
        
        
        lmarca.setBounds(20,0,300,50);
        lugar.add(lmarca);
        
        JTextField marca= new JTextField(30);
        marca.setBounds(190, 15, 200, 25);
        lugar.add(marca);
        
        
        lpos.setBounds(20,30,300,50);
        lugar.add(lpos);
        
        
        pos.setBounds(190, 45, 200, 25);
        llenar();
        
        
        ing.setText("Ingresar");
        ing.setBounds(270,100,120,30);
        lugar.add(ing);
        
        ing.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(marca.getText().length()==0){
                    JOptionPane.showMessageDialog(null, "Ingrese marca del auto.");
                }
                else{
                    a.ingresar((Integer)pos.getSelectedItem(), marca.getText());
                    lugar.dispose();
                }
                
            }
        });
        
    }
    
    public void llenar(){
        int n=0;

        for(int i=1; i<=6; i++){
            for(int j=0; j<a.l.size(); j++){
                if(i==a.l.get(j)){
                    n=1;
                }
            }
            if(n!=1)
                pos.addItem(i);
            n=0;
        }
        lugar.add(pos);
    }
    
    
    
}
